import linkedlist


# Create a new list
print("Creating a new empty list")
l = linkedlist.LinkedList()

# Add some nodes to the list
print("Adding 4 nodes to the list (A,B,C,D)")
l.add_front("A")
l.add_front("B")
l.add_front("C")
l.add_front("D")

# Print the size of the list
print("Printing the size of the list")
print(l.count)

# Print The list
print("Printing the list in order")
for item in l:
    print(item)

# Remove 2 nodes from the front
print("Removing 2 nodes from the front of the list")
l.remove_front()
l.remove_front()


# Print the size of the list
print("Printing the size of the list")
print(l.count)

# Print The list
print("Printing the list in order")
for item in l:
    print(item)

# Remove the rest of the nodes
print("Removing the rest of the nodes")
while l.count > 0:
    l.remove_front()

# Done Testing
print("Done Testing")
