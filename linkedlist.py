class Node:
    # Initialize the node
    def __init__(self, data=None, next=None):
        self.data = data
        self.next = next

    def __repr__(self):
        return str(self.data)


class LinkedList:
    # Initialize the list and node count
    def __init__(self, head=None):
        self.head = head
        self.count = 0

    # String representation of the obj
    def __repr__(self):
        return "<Linked List of size: {0}>".format(self.count)

    # Make the list iterable
    def __iter__(self):
        node = self.head
        while node:
            yield node
            node = node.next

    # Add a node at the front of the list
    def add_front(self, data):
        self.count += 1
        node = Node(data)
        node.next = self.head
        self.head = node

    # Remove a node from the front of the list
    def remove_front(self):
        if self.count > 0:
            self.count -= 1
            delete_node = self.head
            self.head = self.head.next
            del delete_node
        else:
            print("No More Elements in the List")

            # Return the size of the list

    def size(self):
        return self.count
